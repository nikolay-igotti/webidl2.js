export { parse } from "./lib/webidl2.js";
export { write } from "./lib/writer.js";
export { validate } from "./lib/validator.js";
export { WebIDLParseError } from "./lib/tokeniser.js";

import { parse } from "./lib/webidl2.js";
import { write } from "./lib/writer.js";
import { validate } from "./lib/validator.js";

if (false) {
const tree = parse(`
   package "com.example.foo";
   interface Foo {};
`);
    const text = write(tree);
    const validation = validate(tree);
    console.log(text)
    console.log(tree[0])
}
