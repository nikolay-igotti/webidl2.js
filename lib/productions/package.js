import { Base } from "./base.js";
import { autoParenter } from "./helpers.js";

export class Package extends Base {
  static parse(tokeniser) {
    const packageKeyword = tokeniser.consume("package");
    if (!packageKeyword) {
      return;
    }
    const tokens = { base: packageKeyword };
    tokens.clause =
      tokeniser.consumeKind("string") ||
      tokeniser.consumeKind("identifier") ||
      tokeniser.error("No clause for package");
    tokens.termination =
      tokeniser.consume(";") || tokeniser.error("No semicolon after package");
    const ret = autoParenter(new Package({ source: tokeniser.source, tokens }));
    ret.clause = tokens.clause.value;
    return ret.this;
  }

  get type() {
    return "package";
  }

  write(w) {
    return w.ts.definition(
      w.ts.wrap([
        w.token(this.tokens.base),
        w.reference_token(this.tokens.clause, this),
        w.token(this.tokens.termination),
      ]),
      { data: this },
    );
  }
}
