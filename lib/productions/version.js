import { Base } from "./base.js";
import { ExtendedAttributes } from "./extended-attributes.js";
import { autoParenter } from "./helpers.js";

export class Version extends Base {
  /**
   * @param {import("../tokeniser.js").Tokeniser} tokeniser
   */
  static parse(tokeniser) {
    const startTokeniserPosition = tokeniser.position;
    const extAttrs = ExtendedAttributes.parse(tokeniser);

    /** @type {Base["tokens"]} */
    const tokens = {};
    tokens.base = tokeniser.consume("version");
    if (!tokens.base) {
      tokeniser.unconsume(startTokeniserPosition);
      return;
    }

    let value = "";
    for (let i = 0; i < 10; ++i) {
      tokens.termination = tokeniser.consume(";");
      if (tokens.termination) {
        break;
      }
      const token =
        tokeniser.consumeKind("decimal") ||
        tokeniser.consumeKind("integer") ||
        tokeniser.consumeKind("identifier") ||
        tokeniser.consumeKind("other") ||
        tokeniser.error("version lacks a value");

      tokens[`value${i}`] = token;
      value += token.value;
    }

    if (!tokens.termination || !value) {
      tokeniser.error("Malformed version value");
    }

    const ret = new Version({ source: tokeniser.source, tokens });
    autoParenter(ret).value = value.split(".");
    autoParenter(ret).extAttrs = extAttrs;
    return ret;
  }

  get type() {
    return "version";
  }

  /** @param {import("../writer.js").Writer} w */
  write(w) {
    const { parent } = this;
    const valueTokens = [];
    for (let i = 0; i < 10; ++i) {
      valueTokens.push(w.token(this.tokens[`value${i}`]));
    }
    return w.ts.definition(
      w.ts.wrap([
        this.extAttrs.write(w),
        w.token(this.tokens.base),
        ...valueTokens,
        w.token(this.tokens.termination),
      ]),
      { data: this, parent },
    );
  }
}
