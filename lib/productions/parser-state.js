import { Base } from "./base.js";

const genericsHolder = new Set([
  "FrozenArray",
  "ObservableArray",
  "Promise",
  "sequence",
  "record",
]);

export class ParserState extends Base {
  static get generics() {
    return genericsHolder;
  }

  /**
   * @param {import("../tokeniser.js").Tokeniser} tokeniser
   */
  static update(tokeniser) {
    const startPosition = tokeniser.position;
    do {
      /** @type {Base["tokens"]} */
      const tokens = {};
      tokens.open1 = tokeniser.consume("[");
      if (!tokens.open1) break;

      tokens.open2 = tokeniser.consume("[");
      if (!tokens.open2) break;

      tokens.mode =
        tokeniser.consumeKind("identifier") ||
        tokeniser.error("Parser state mode-identifier expected");

      if (tokens.mode.value === "generics") {
        ParserState.updateGenerics(tokeniser, tokens);
      } else
        tokeniser.error(
          `Parser state mode not supported: ${tokens.mode.value}`,
        );

      tokens.close1 =
        tokeniser.consume("]") ||
        tokeniser.error("Parser state terminator expected");

      tokens.close2 =
        tokeniser.consume("]") ||
        tokeniser.error("Parser state terminator expected");

      return new ParserState({
        source: tokeniser.source,
        tokens,
      });
    } while (false);

    tokeniser.unconsume(startPosition);
  }

  /**
   * @param {import("../tokeniser.js").Tokeniser} tokeniser
   * @param {Base["tokens"]} tokens
   */
  static updateGenerics(tokeniser, tokens) {
    tokens.operation1 =
      tokeniser.consumeKind("other") ||
      tokeniser.error("Parser state generic operation required");
    let operation = tokens.operation1.value;
    tokens.operation2 =
      tokeniser.consume("=") ||
      tokeniser.error("Parser state generic operation required");
    operation += tokens.operation2.value;

    for (let i = 0; ; ++i) {
      if (tokeniser.probe("]")) break;

      const subtype =
        tokeniser.consumeKind("identifier") ||
        tokeniser.error(
          `Parser state generic ${operation} requires an identifier`,
        );

      switch (operation) {
        case "+=":
          ParserState.generics.add(subtype.value);
          break;
        case "-=":
          ParserState.generics.delete(subtype.value);
          break;
        default:
          tokeniser.error(
            `Unknown parser state generic operation: ${operation}`,
          );
      }
      const delimeter = tokeniser.consume(",");

      tokens[`subtype_${i}`] = subtype;
      tokens[`delimeter_${i}`] = delimeter;

      if (!delimeter) break;
    }
  }

  /** @param {import("../writer.js").Writer} w */
  write(w) {
    return w.ts.wrap([
      ...Object.values(this.tokens).map((token) => w.token(token)),
    ]);
  }
}
