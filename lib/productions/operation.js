import { Base } from "./base.js";
import { argumentNameKeywords } from "../tokeniser.js";
import {
  return_type,
  argument_list,
  unescape,
  autoParenter,
} from "./helpers.js";
import { validationError } from "../error.js";

export class Operation extends Base {
  /**
   * @param {import("../tokeniser.js").Tokeniser} tokeniser
   * @param {object} [options]
   * @param {import("../tokeniser.js").Token} [options.special]
   * @param {boolean} [options.regular]
   * @param {boolean} [options.noneOnFail]
   */
  static parse(tokeniser, { special, regular, noneOnFail } = {}) {
    const begin_position = tokeniser.position;
    const tokens = { special };
    const ret = autoParenter(
      new Operation({ source: tokeniser.source, tokens }),
    );
    if (special && special.value === "stringifier") {
      tokens.termination = tokeniser.consume(";");
      if (tokens.termination) {
        ret.arguments = [];
        return ret;
      }
    }
    if (!special && !regular) {
      tokens.special = tokeniser.consume("getter", "setter", "deleter");
    }
    const async_position = tokeniser.position;
    tokens.async = tokeniser.consume("async");
    if (tokens.async && tokens.special && tokens.special.value !== "static") {
      if (noneOnFail) {
        tokeniser.unconsume(begin_position);
        return;
      } else {
        tokeniser.unconsume(async_position);
        tokeniser.error("Async is not allowed here");
      }
    }
    ret.idlType = return_type(tokeniser);
    if (!ret.idlType) {
      if (noneOnFail) {
        tokeniser.unconsume(begin_position);
        return;
      } else tokeniser.error("Missing return type");
    }
    tokens.name =
      tokeniser.consumeKind("identifier") ||
      tokeniser.consume(...argumentNameKeywords);
    tokens.open = tokeniser.consume("(");
    if (!tokens.open) {
      if (noneOnFail) {
        tokeniser.unconsume(begin_position);
        return;
      } else tokeniser.error("Invalid operation");
    }
    ret.arguments = argument_list(tokeniser);
    tokens.close = tokeniser.consume(")");
    if (!tokens.close) {
      if (noneOnFail) {
        tokeniser.unconsume(begin_position);
        return;
      } else tokeniser.error("Unterminated operation");
    }
    tokens.termination = tokeniser.consume(";");
    if (!tokens.termination) {
      if (noneOnFail) {
        tokeniser.unconsume(begin_position);
        return;
      } else tokeniser.error("Unterminated operation, expected `;`");
    }
    return ret.this;
  }

  get type() {
    return "operation";
  }
  get name() {
    const { name } = this.tokens;
    if (!name) {
      return "";
    }
    return unescape(name.value);
  }
  get special() {
    if (!this.tokens.special) {
      return "";
    }
    return this.tokens.special.value;
  }
  get async() {
    return !!this.tokens.async;
  }

  *validate(defs) {
    yield* this.extAttrs.validate(defs);
    if (!this.name && ["", "static"].includes(this.special)) {
      const message = `Regular or static operations must have both a return type and an identifier.`;
      yield validationError(this.tokens.open, this, "incomplete-op", message);
    }
    if (this.async && this.special && this.special !== "static") {
      const message = `Operations with ${this.special} special must not be async.`;
      yield validationError(this.tokens.open, this, "incomplete-op", message);
    }
    if (this.idlType) {
      yield* this.idlType.validate(defs);
    }
    for (const argument of this.arguments) {
      yield* argument.validate(defs);
    }
  }

  /** @param {import("../writer.js").Writer} w */
  write(w) {
    const { parent } = this;
    const body = this.idlType
      ? [
          w.ts.type(this.idlType.write(w)),
          w.name_token(this.tokens.name, { data: this, parent }),
          w.token(this.tokens.open),
          w.ts.wrap(this.arguments.map((arg) => arg.write(w))),
          w.token(this.tokens.close),
        ]
      : [];
    return w.ts.definition(
      w.ts.wrap([
        this.extAttrs.write(w),
        this.tokens.name
          ? w.token(this.tokens.special)
          : w.token(this.tokens.special, w.ts.nameless, { data: this, parent }),
        w.token(this.tokens.async),
        ...body,
        w.token(this.tokens.termination),
      ]),
      { data: this, parent },
    );
  }
}
