import { Base } from "./base.js";
import { autoParenter } from "./helpers.js";

export class Import extends Base {
  static parse(tokeniser) {
    const importKeyword = tokeniser.consume("import");
    if (!importKeyword) {
      return;
    }
    const tokens = { import: importKeyword };
    tokens.clause =
      tokeniser.consumeKind("string") ||
      tokeniser.consumeKind("identifier") ||
      tokeniser.error("Incomplete import statement");
    tokens.as = tokeniser.consume("as");
    if (tokens.as)
      tokens.alias =
        tokeniser.consumeKind("identifier") ||
        tokeniser.error("Incomplete import statement");
    tokens.termination =
      tokeniser.consume(";") ||
      tokeniser.error("No terminating ; for import statement");
    const ret = autoParenter(new Import({ source: tokeniser.source, tokens }));
    ret.clause = tokens.clause.value;
    ret.alias = tokens.alias?.value;
    return ret.this;
  }

  get type() {
    return "import";
  }

  write(w) {
    return w.ts.definition(
      w.ts.wrap([
        w.token(this.tokens.import),
        w.reference_token(this.tokens.clause, this),
        w.token(this.tokens.as),
        w.token(this.tokens.alias),
        w.token(this.tokens.termination),
      ]),
      { data: this },
    );
  }
}
