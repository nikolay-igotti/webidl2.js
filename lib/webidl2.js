import { Tokeniser } from "./tokeniser.js";
import { Enum } from "./productions/enum.js";
import { Includes } from "./productions/includes.js";
import { Package } from "./productions/package.js";
import { Version } from "./productions/version.js";
import { Import } from "./productions/import.js";
import { ExtendedAttributes } from "./productions/extended-attributes.js";
import { Typedef } from "./productions/typedef.js";
import { Dictionary } from "./productions/dictionary.js";
import { Namespace } from "./productions/namespace.js";
import { Eof } from "./productions/token.js";
import { ParserState } from "./productions/parser-state.js";
import { Attribute } from "./productions/attribute.js";
import { Constant } from "./productions/constant.js";
import { Operation } from "./productions/operation.js";
import {
  autoParenter,
  parseCallback,
  parsePartial,
  parseInterface,
} from "./productions/helpers.js";

/** @typedef {import("./productions/helpers.js").ParserOptions} ParserOptions */

/**
 * @param {Tokeniser} tokeniser
 * @param {ParserOptions} options
 */
function parseByTokens(tokeniser, options) {
  const source = tokeniser.source;

  function definition() {
    if (options.productions) {
      for (const production of options.productions) {
        const result = production(tokeniser);
        if (result) {
          return result;
        }
      }
    }

    return (
      parseCallback(tokeniser, options) ||
      parseInterface(tokeniser, options) ||
      parsePartial(tokeniser, options) ||
      Dictionary.parse(tokeniser, options?.extensions?.dictionary) ||
      Enum.parse(tokeniser) ||
      Typedef.parse(tokeniser) ||
      Includes.parse(tokeniser) ||
      Package.parse(tokeniser) ||
      Import.parse(tokeniser) ||
      Namespace.parse(tokeniser, options?.extensions?.namespace) ||
      Version.parse(tokeniser) ||
      Attribute.parse(tokeniser, { noInherit: true, readonly: true }) ||
      Constant.parse(tokeniser) ||
      Operation.parse(tokeniser, { regular: true, noneOnFail: true })
    );
  }

  function definitions() {
    if (!source.length) return [];
    const defs = [];
    while (true) {
      const psu1 = ParserState.update(tokeniser);
      if (psu1) defs.push(psu1);
      const ea = ExtendedAttributes.parse(tokeniser);
      const def = definition();
      if (!def) {
        if (ea.length) tokeniser.error("Stray extended attributes");
        break;
      }
      autoParenter(def).extAttrs = ea;
      defs.push(def);

      const psu2 = ParserState.update(tokeniser);
      if (psu2) defs.push(psu2);
    }
    const eof = Eof.parse(tokeniser);
    if (options.concrete) {
      defs.push(eof);
    }
    return defs;
  }

  const res = definitions();
  if (tokeniser.position < source.length)
    tokeniser.error("Unrecognised tokens");
  return res;
}

/**
 * @param {string} str
 * @param {ParserOptions} [options]
 */
export function parse(str, options = {}) {
  const tokeniser = new Tokeniser(str);
  if (typeof options.sourceName !== "undefined") {
    // @ts-ignore (See Tokeniser.source in supplement.d.ts)
    tokeniser.source.name = options.sourceName;
  }
  return parseByTokens(tokeniser, options);
}
