[[generics += Readonly, Matrix]]

interface Foo {
  Readonly<Matrix<sequence<DOMString?>, string, number>, undefined> bar();
  attribute Matrix<string> baz;

  [[generics += Gnr1, Gnr2, Gnr3]]
};

namespace boo {
  Gnr1<Gnr2<Gnr3<undefined, string, Foo>, Foo>, Gnr3<string>> func();
};
