// NOTE!!! no escaping/unescaping is performed by webidl2.js parser/writer/validator

enum _AltMealType { "ri\"ce", "n'o`o\"\"\"dles", "o\x00\0ther", };

dictionary PaintOptions {
  DOMString? fillPattern = "b\"\"\"'1```lack";
};

[Exposed=Window]
interface I {
    [ATextWithDoubleQuotes="
        \'      single quote    byte 0x27 in ASCII encoding
        \"      double quote    byte 0x22 in ASCII encoding
        \\      backslash       byte 0x5c in ASCII encoding
        \b      backspace       byte 0x08 in ASCII encoding
        \f      form feed - new page    byte 0x0c in ASCII encoding
        \n      line feed - new line    byte 0x0a in ASCII encoding
        \r      carriage return byte 0x0d in ASCII encoding
        \t      horizontal tab  byte 0x09 in ASCII encoding
        \v      vertical tab    byte 0x0b in ASCII encoding
        \1      arbitrary octal value   code unit nnn (1~3 octal digits)
        \12     arbitrary octal value   code unit nnn (1~3 octal digits)
        \123    arbitrary octal value   code unit nnn (1~3 octal digits)
        \xff    2-hex
        \u1234  4-unicode
    "]
    attribute string attr;
};
