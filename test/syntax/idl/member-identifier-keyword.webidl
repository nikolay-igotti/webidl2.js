interface I {
  attribute DOMString callback;
  void callback(number interface, string string);
};

dictionary D {
    number callback = 0;
};
