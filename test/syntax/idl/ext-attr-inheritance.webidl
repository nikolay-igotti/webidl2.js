interface IBase {};
interface IDerived : [SomeExtAttr=someValue] IBase {};

dictionary DBase {};
dictionary DDerived : [SomeExtAttr=someValue] DBase {};
