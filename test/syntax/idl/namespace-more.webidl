namespace ns {
    namespace ns2 {namespace ns3 {};};

    partial namespace ns2p {};
    partial interface Foo {attribute DOMString quux;};
    readonly attribute DOMString quux;

    callback SortCallback = boolean (any a, any b);
    interface Animal {attribute DOMString name;};

    dictionary _A {
        long h;
        long d;
    };

    enum _AltMealType { "rice", "noodles", "other", };

    typedef sequence<Point> PointSequence;
};
