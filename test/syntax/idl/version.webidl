
////////////////////////////////////////
version 127;
version 127.220;
version 127.220.380;
version 127.220.380.440;

version aa.bb.cc.dd;
version -.b-.-c.d-d;
version 1aa2.3bb4.5cc6.7dd8;

////////////////////////////////////////
[ea=v]
version abi.feature.fix.build;

////////////////////////////////////////
namespace ns {
    version 1.2.3-dev456;
};
