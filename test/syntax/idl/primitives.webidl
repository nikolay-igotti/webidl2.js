interface Primitives {
  attribute boolean truth;
  attribute byte character;
  attribute octet value;
  attribute short myNumber; // (NOTE: the identifier "number" is a keyword for the idlzer needs at now)
  attribute unsigned short positive;
  attribute long big;
  attribute unsigned long bigpositive;
  attribute long long bigbig;
  attribute  unsigned long long bigbigpositive;
  attribute float real;
  attribute double bigreal;
  attribute unrestricted float realwithinfinity;
  attribute unrestricted double bigrealwithinfinity;
  attribute DOMString string;
  attribute ByteString bytes;
  attribute Date date;
  attribute RegExp regexp;
};
