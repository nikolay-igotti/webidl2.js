interface Service {
  getter async DOMString lookup(DOMString key);
};