interface Service {
  async getter DOMString lookup(DOMString key);
};